import app, { appState } from './app';
import github, { githubState } from './github';
import user, { userState } from './user';
import roomSlice from '../redux/slices/RoomSlice';

export const initialState = {
  app: appState,
  github: githubState,
  user: userState,
  room: roomSlice,
};

export default {
  ...app,
  ...github,
  ...user,
  ...roomSlice,
};
