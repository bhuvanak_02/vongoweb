import React from 'react';
import { Helmet } from 'react-helmet-async';
import { Switch, Route, Router, Redirect } from 'react-router-dom';
import styled, { ThemeProvider } from 'styled-components';
import history from 'modules/history';
import theme from 'modules/theme';
import config from 'config';
import Layout from 'containers/Layout/Layout';
import Signup from 'containers/Auth/Signup/Signup';
import Rooms from 'containers/Rooms/Rooms';
import ExploreRooms from 'containers/Explore/Rooms/Rooms';
import ExploreClubs from 'containers/Explore/Clubs/Clubs';
import ExplorePeoples from 'containers/Explore/People/People';

const AppWrapper = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
  opacity: 1 !important;
  position: relative;
  transition: opacity 0.5s;
`;

function Root() {

  const ExploreRoute = ({ match }) => {
    return (
      <Switch>
        <Route path={`${match.path}/rooms`} component={ExploreRooms} />
        <Route path={`${match.path}/clubs`} component={ExploreClubs} />
        <Route path={`${match.path}/people`} component={ExplorePeoples} />
      </Switch>
    )
  }

  const PreferenceRoute = ({ match }) => {
    return (
      <Switch>
        <Route path={`${match.path}/tags`} render={() => <p>Preferences -&gt; Tags</p>} />
        <Route path={`${match.path}/peopleandclubs`} render={() => <p>Preferences -&gt; People and Clubs</p>} />
      </Switch>
    )
  }

  const AuthRoute = ({ match }) => {
    console.log(match.path);

    return (<Switch>
      <Route path={`${match.path}/signup`} component={Signup} />
      {/* <Route path={`${match.path}/signup`} component={} /> */}
    </Switch>)
  }

  return (
    <Router history={history}>
      <ThemeProvider theme={theme}>
        <Helmet
          defaultTitle={config.name}
          defer={false}
          encodeSpecialCharacters
          htmlAttributes={{ lang: 'en-US' }}
          titleAttributes={{ itemprop: 'name', lang: 'en-US' }}
          titleTemplate={`%s | ${config.name}`}
        >
          <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
          />
          <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/icon?family=Material+Icons"
          />
        </Helmet>
        <Layout>
          <Switch>
            <Route path="/" exact render={() => <Redirect to="/rooms" />} />
            <Route path="*/auth" component={AuthRoute} />
            <Route path="/rooms" component={Rooms} />
            <Route path="/explore" component={ExploreRoute} />
            <Route path="/start-a-room" render={() => <p>Start a Room</p>} />
            <Route path="/chat" render={() => <p>Chat</p>} />
            <Route path="/preferences" component={PreferenceRoute} />
          </Switch>
        </Layout>
      </ThemeProvider>
    </Router>
  );
}

export default Root;
