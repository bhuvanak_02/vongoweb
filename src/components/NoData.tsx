import React from 'react';
import styled from 'styled-components';

export const Wrapper = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    margin-top: 100px;
`;

export const TextMessage = styled.p`
    text-align: center;
`;

function NoData({message}) {
  return (
    <Wrapper>
     <TextMessage>{message}</TextMessage>
    </Wrapper>
  );
}

export default NoData;
