import React from 'react';
import { Stack, Button, IconButton } from '@mui/material';
import NotificationsIcon from '@mui/icons-material/Notifications';
import SettingsIcon from '@mui/icons-material/Settings';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import MenuIcon from '@mui/icons-material/Menu';
import { makeStyles } from '@mui/styles';
import { withRouter } from 'react-router-dom';
import { push } from 'modules/history';
import './styles.scss';

const useStyles = makeStyles({
  btnSignUp: {
    backgroundColor: '#FD8957 !important',
    borderRadius: '20px !important',
    padding: '0 16px !important',
    'text-transform': 'none !important',
    fontSize: '16px !important',
    'font-weight': '400 !important'
  },
  icon: {
    fontSize: '24px !important',
    padding: '0 !important',
    color: '#FFFFFF !important'
  }
});

function Header({
  showDrawerToggle,
  onToggleSidebar,
  location: { pathname }
}) {

  const classes = useStyles();

  function handleAuthClick() {
    console.log('Pathname : ', pathname);
    push(pathname + '/auth/signup');
  }

  return (
    <div className="header">
      {
        showDrawerToggle ? (
          <IconButton
            className={classes.icon}
            aria-label="toggle-sidebar"
            onClick={onToggleSidebar}
          >
            <MenuIcon fontSize="inherit" />
          </IconButton>
        ) : <span></span>
      }
      <Stack direction="row" spacing={1}>
        <AccountCircleIcon style={{fontSize: '30px'}} />
        <Button
          className={classes.btnSignUp}
          variant="contained"
          onClick={handleAuthClick}
        >
          Sign up
        </Button>
        <IconButton className={classes.icon}>
          <NotificationsIcon fontSize="inherit" />
        </IconButton>
        <IconButton className={classes.icon}>
          <SettingsIcon fontSize="inherit" />
        </IconButton>
      </Stack>
    </div>
  );
}

export default withRouter(Header);