import React from 'react';
import {
    Drawer,
    List,
    ListItemButton,
    ListItemText,
    Collapse,
    Typography,
    Box
} from '@mui/material';
import { NavLink, useLocation } from 'react-router-dom';
import { makeStyles } from '@mui/styles';
import Logo from './vongo_logo.png';
import './styles.scss';

const useListItemStyles = makeStyles({
    primary: {
        fontSize: '16px !important',
        color: '#FFFFFF',
        '&.active': {
            color: '#FD8957'
        }
    }
});

export default function Sidebar({
    showDrawerToggle,
    showDrawer,
    onToggleSidebar
}) {   
    const { pathname } = useLocation();
    const listItemClasses = useListItemStyles();

    function isPathActive(path) {
        return pathname.startsWith(path);
    }

    const SidebarContent = ({ onToggleSidebar }) => (
        <div className="sidebar" onClick={onToggleSidebar}>
            <div className="logo-container">
                <a href="/">
                    <img src={Logo} alt='Vongo' className="logo" />
                </a>
            </div>
            <List component="nav" aria-label="main mailbox folders">
                <ListItemButton component={NavLink} to="/rooms">
                    <ListItemText primary={<Typography variant="body2" className={`${listItemClasses.primary} ${isPathActive('/rooms') ? 'active' : ''}`}>Rooms</Typography>} />
                </ListItemButton>
                <ListItemButton component={NavLink} to="/explore/rooms">
                    <ListItemText primary={<Typography variant="body2" className={`${listItemClasses.primary} ${isPathActive('/explore') ? 'active' : ''}`}>Explore</Typography>} />
                </ListItemButton>
                <Collapse in={isPathActive('/explore')} timeout="auto" unmountOnExit>
                    <List component="div" disablePadding>
                    <ListItemButton sx={{ pl: 4 }} component={NavLink} to="/explore/rooms">
                        <ListItemText primary={<Typography variant="body2" className={`${listItemClasses.primary} ${isPathActive('/explore/rooms') ? 'active' : ''}`}>Rooms</Typography>} />
                    </ListItemButton>
                    <ListItemButton sx={{ pl: 4 }} component={NavLink} to="/explore/clubs">
                        <ListItemText primary={<Typography variant="body2" className={`${listItemClasses.primary} ${isPathActive('/explore/clubs') ? 'active' : ''}`}>Clubs</Typography>} />
                    </ListItemButton>
                    <ListItemButton sx={{ pl: 4 }} component={NavLink} to="/explore/people">
                        <ListItemText primary={<Typography variant="body2" className={`${listItemClasses.primary} ${isPathActive('/explore/people') ? 'active' : ''}`}>People</Typography>} />
                    </ListItemButton>
                    </List>
                </Collapse>
                <ListItemButton component={NavLink} to="/start-a-room/open">
                    <ListItemText primary={<Typography variant="body2" className={`${listItemClasses.primary} ${isPathActive('/start-a-room') ? 'active' : ''}`}>Start a Room</Typography>} />
                </ListItemButton>
                <Collapse in={isPathActive('/start-a-room')} timeout="auto" unmountOnExit>
                    <List component="div" disablePadding>
                    <ListItemButton sx={{ pl: 4 }} component={NavLink} to="/start-a-room/open">
                        <ListItemText primary={<Typography variant="body2" className={`${listItemClasses.primary} ${isPathActive('/start-a-room/open') ? 'active' : ''}`}>Open Rooms</Typography>} />
                    </ListItemButton>
                    <ListItemButton sx={{ pl: 4 }} component={NavLink} to="/start-a-room/event">
                        <ListItemText primary={<Typography variant="body2" className={`${listItemClasses.primary} ${isPathActive('/start-a-room/event') ? 'active' : ''}`}>Event Rooms</Typography>} />
                    </ListItemButton>
                    </List>
                </Collapse>
                <ListItemButton component={NavLink} to="/chat">
                    <ListItemText primary={<Typography variant="body2" className={`${listItemClasses.primary} ${isPathActive('/chat') ? 'active' : ''}`}>Chat</Typography>} />
                </ListItemButton>
                <ListItemButton component={NavLink} to="/preferences/tags">
                    <ListItemText primary={<Typography variant="body2" className={`${listItemClasses.primary} ${isPathActive('/preferences') ? 'active' : ''}`}>Preferences</Typography>} />
                </ListItemButton>
                <Collapse in={isPathActive('/preferences')} timeout="auto" unmountOnExit>
                    <List component="div" disablePadding>
                        <ListItemButton sx={{ pl: 4 }} component={NavLink} to="/preferences/tags">
                            <ListItemText primary={<Typography variant="body2" className={`${listItemClasses.primary} ${isPathActive('/preferences/tags') ? 'active' : ''}`}>Tags</Typography>} />
                        </ListItemButton>
                        <ListItemButton sx={{ pl: 4 }} component={NavLink} to="/preferences/peopleandclubs">
                            <ListItemText primary={<Typography variant="body2" className={`${listItemClasses.primary} ${isPathActive('/preferences/peopleandclubs') ? 'active' : ''}`}>People and Clubs</Typography>} />
                        </ListItemButton>
                    </List>
                </Collapse>
            </List>
        </div>
    )

    return showDrawerToggle ? (
        <>
            <Drawer
                anchor="left"
                open={showDrawer}
                onClose={onToggleSidebar}
            >
                <SidebarContent onToggleSidebar={onToggleSidebar} />
            </Drawer>
        </>
    ) : <SidebarContent />
}