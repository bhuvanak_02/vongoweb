import * as React from "react";
import Box from "@mui/material/Box";
import SvgIcon from "@mui/material/SvgIcon";
import moment from "moment";
import "./styles.scss";

function HomeIcon(props) {
  return (
    <SvgIcon {...props}>
      <path d="M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z" />
    </SvgIcon>
  );
}

const ListItem = (props) => {
  const { category, event, title, dateTime, onClick, roomId } = props;

  const date = new Date(dateTime);
  const getDateTime = moment(date).format('MMM DD, YYYY HH:mm:ss A');

  const handleOnClick = () => {
    if (onClick) {
      onClick(roomId);
    }
  }

  return (
    <div className="container" onClick={handleOnClick}>
      <div className="row-category">
        <div className="row-style">{category}</div>
        <Box>
          <HomeIcon sx={{ color: "#fff", fontSize: 20, paddingLeft: '5px' }} />
        </Box>
      </div>
      <div className="row-style">{event}</div>
      <div className="row-category">
        <div className="row-style">{title}</div>
        <div className="row-date-style">{getDateTime}</div>
      </div>
    </div>
  );
};

export default ListItem;
