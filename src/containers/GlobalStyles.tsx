import React from 'react';
import { createGlobalStyle } from 'styled-components';

import { appColor } from 'modules/theme';

const GlobalStyle = createGlobalStyle`
  *,
  *:before,
  *:after {
    box-sizing: border-box;
  }

  html {
    font-size: 62.5%;
    -webkit-font-smoothing: antialiased;
    height: 100%;
  }

  body {
    font-family: Lato, sans-serif;
    font-size: 16px; /* stylelint-disable unit-disallowed-list */
    margin: 0;
    min-height: 100vh;
    padding: 0;
    color: #FFFFFF;
  }

  img {
    height: auto;
    max-width: 100%;
  }

  a {
    color: ${appColor};
    text-decoration: none;

    &.disabled {
      pointer-events: none;
    }
  }

  button {
    appearance: none;
    background-color: transparent;
    border: 0;
    cursor: pointer;
    display: inline-block;
    font-family: inherit;
    line-height: 1;
    padding: 0;
  }

  ::-webkit-scrollbar {
    width: 9px;
  }
  
  ::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
    background: linear-gradient(180deg, rgba(74, 74, 74, 0.7) 0%, rgba(74, 74, 74, 0) 100%);
    border-radius: 10px;
  }
  
  ::-webkit-scrollbar-thumb {
    border-radius: 10px;
    background: rgba(253, 137, 87, 1);
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.5); 
  }
`;

export default function GlobalStyles() {
  return <GlobalStyle />;
}
