import * as React from "react";
import { useDispatch, useSelector } from 'react-redux';
import {
    List,
    ListItem,
    Card,
    CardContent,
    Typography,
    Container,
    Stack,
    Avatar
} from '@mui/material';
import { useResponsiveScreenState } from 'modules/hooks';

import NoData from "../../../components/NoData";
import { getPeoplesList } from '../../../redux/slices/PeopleSlice';

export default function ExplorePeople() {
    const dispatch = useDispatch();
    const peoplesList = useSelector(state => state?.people?.peoplesList);
    const initialRender = useSelector(
        state => state?.people?.initialRender,
    );
    const { isDesktop } = useResponsiveScreenState();

    React.useEffect(() => {
        dispatch(getPeoplesList());
        !initialRender && dispatch(getPeoplesList());
    }, [dispatch, initialRender]);

    function renderListItem({
        userName
    }) {
        const str = userName != "string" && userName != null ? userName.toUpperCase() : '';
        console.log(str)
        const firstChar = str.charAt(0);
        return (
            <Card sx={{ width: '100%', backgroundColor: 'rgba(196, 196, 196, 0.1)', color: '#FFFFFF' }}>
                <CardContent sx={{ display: 'flex' }}>
                    <Stack
                        direction="row"
                        spacing={2}
                        alignItems="center"
                    >
                        <Avatar sx={{ bgcolor: '#FD8957', width: 30, height: 30 }}>{firstChar}</Avatar>
                        <Typography sx={{ fontSize: 16, fontWeight: '700 !important' }}>{userName}</Typography>
                    </Stack>
                </CardContent>
            </Card>
        );
    }

    if (peoplesList.length === 0) {
        return <NoData message="No Peoples Found" />
    }

    return (
        <Container sx={{
            ml: 0,
            boxShadow: '19px 52px 100px 0px rgba(0, 0, 0, 0.25)',
            borderRadius: '29px 0 0 29px',
            padding: isDesktop ? '24px' : '0',
            maxHeight: '720px',
            overflowY: 'auto',
            cursor: 'pointer'
        }}>
            <List>
                {
                    peoplesList.map((room, index) => (
                        <ListItem key={index}>
                            {renderListItem(room)}
                        </ListItem>
                    ))
                }
            </List>
        </Container>
    )
}
