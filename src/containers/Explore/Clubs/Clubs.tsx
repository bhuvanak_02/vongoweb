import * as React from "react";
import { useDispatch, useSelector } from 'react-redux';
import {
    List,
    ListItem,
    Card,
    CardContent,
    Typography,
    Container,
    Stack,
    Avatar
} from '@mui/material';
import { useResponsiveScreenState } from 'modules/hooks';

import NoData from "../../../components/NoData";
import { getClubsList } from '../../../redux/slices/ClubsSlice';

export default function ExploreClubs() {
    const dispatch = useDispatch();
    const clubsList = useSelector(state => state?.clubs?.clubsList);
    const initialRender = useSelector(
        state => state?.clubs?.initialRender,
    );
    const { isDesktop } = useResponsiveScreenState();

    React.useEffect(() => {
        dispatch(getClubsList());
        !initialRender && dispatch(getClubsList());
    }, [dispatch, initialRender]);

    function renderListItem({
        subCategoryName,
        title,
        memberIds
    }) {
        const memberId = memberIds != null && memberIds.length > 1 ? 'MEMBERS' : 'MEMBER';
        const MEMBER = `${memberIds != null && memberIds.length ? memberIds.length : 0} ${memberId}`;
        const str = title;
        const firstChar = str.charAt(0);
        return (
            <Card sx={{ width: '100%', backgroundColor: 'rgba(196, 196, 196, 0.1)', color: '#FFFFFF' }}>
                <CardContent sx={{ display: 'flex' }}>
                    <Stack>
                        <Avatar sx={{ bgcolor: '#FD8957', width: 30, height: 30 }}>{firstChar}</Avatar>
                    </Stack>
                    <Stack
                        sx={{ pl: 2 }}>
                        <Typography sx={{ fontSize: 16, fontWeight: '700 !important' }}>{title}</Typography>
                        <Typography sx={{ fontSize: 16, fontWeight: '700 !important' }}>{subCategoryName}</Typography>
                        <Typography sx={{ fontSize: 14, fontWeight: 100 }}>{MEMBER}</Typography>
                    </Stack>
                </CardContent>
            </Card>
        );
    }

    if (clubsList.length === 0) {
        return <NoData message="No Clubs Found" />
    }

    return (
        <Container sx={{
            ml: 0,
            boxShadow: '19px 52px 100px 0px rgba(0, 0, 0, 0.25)',
            borderRadius: '29px 0 0 29px',
            padding: isDesktop ? '24px' : '0',
            maxHeight: '720px',
            overflowY: 'auto',
            cursor: 'pointer'
        }}>
            <List>
                {
                    clubsList.map((room, index) => (
                        <ListItem key={index}>
                            {renderListItem(room)}
                        </ListItem>
                    ))
                }
            </List>
        </Container>
    )
}
