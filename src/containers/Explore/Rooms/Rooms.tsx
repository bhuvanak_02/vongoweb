import * as React from "react";
import { useDispatch, useSelector } from 'react-redux';
import {
  List,
  ListItem,
  Card,
  CardContent,
  Typography,
  Container,
  Stack
} from '@mui/material';
import HomeIcon from '@mui/icons-material/Home';
import moment from "moment";
import { useResponsiveScreenState } from 'modules/hooks';

import NoData from "../../../components/NoData";
import { getRoomsList } from '../../../redux/slices/RoomSlice';

export default function ExploreRooms() {
  const dispatch = useDispatch();
  const roomsList = useSelector(state => state?.room?.roomsListData);
  const initialRender = useSelector(
    state => state?.room?.initialRender,
  );
  const { isDesktop } = useResponsiveScreenState();

  React.useEffect(() => {
    dispatch(getRoomsList());
    !initialRender && dispatch(getRoomsList());
  }, [dispatch, initialRender]);

  function getDateTime(dateStr) {
    return moment(dateStr).format('MMM DD, YYYY HH:mm:ss A') || '';
  }

  const roomsListData = roomsList.filter((list) => list.status === 'ONGOING' || 'UPCOMING');

  function renderListItem({
    subCategoryName,
    title,
    clubName,
    startTime
  }) {
    const categoryName = clubName ? clubName : subCategoryName;
    return (
      <Card sx={{ width: '100%', backgroundColor: 'rgba(196, 196, 196, 0.1)', color: '#FFFFFF' }}>
        <CardContent>
          <Stack
            direction="row"
            spacing={2}
            alignItems="center"
          >
            <Typography sx={{ fontSize: 16, fontWeight: '700 !important' }}>{ categoryName }</Typography>
            {clubName && (
              <HomeIcon />
            )}
          </Stack>
          <Typography sx={{ fontSize: 16, fontWeight: '700 !important' }}>{ title }</Typography>
          <Stack
            direction="row"
            spacing={2}
            alignItems="center"
            sx={{ mt: 1 }}
          >
            <Typography sx={{ fontSize: 14, fontWeight: 100 }}>{ getDateTime(startTime) }</Typography>
          </Stack>
        </CardContent>
      </Card>
    );
  }

  if (roomsListData.length === 0) {
    return <NoData message="No Rooms Found" />
  }

  return (
    <Container sx={{
      ml: 0,
      boxShadow: '19px 52px 100px 0px rgba(0, 0, 0, 0.25)',
      borderRadius: '29px 0 0 29px',
      padding: isDesktop ? '24px' : '0',
      maxHeight: '720px',
      overflowY: 'auto',
      cursor: 'pointer'
    }}>
      <List>
        {
          roomsListData.map((room, index) => (
            <ListItem key={index}>
              { renderListItem(room) }
            </ListItem>
          ))
        }
      </List>
    </Container>
  )
}
