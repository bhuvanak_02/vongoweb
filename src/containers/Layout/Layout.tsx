import React, { useState } from 'react';
import Sidebar from 'components/Sidebar/Sidebar';
import Header from 'components/Header/Header';
import { useResponsiveScreenState } from 'modules/hooks';
import './styles.scss';

function Layout({ children }) {
    const [showMobileSidebar, setShowMobileSidebar] = useState(false);
    const { isDesktop } = useResponsiveScreenState();

    function handleToggleSidebar() {
        setShowMobileSidebar(!showMobileSidebar);
    }

    return (
        <div className="layout-container">
            <Sidebar
                showDrawerToggle={!isDesktop}
                showDrawer={showMobileSidebar}
                onToggleSidebar={handleToggleSidebar}
            />
            <div className="main">
                <Header
                    showDrawerToggle={!isDesktop}
                    onToggleSidebar={handleToggleSidebar}
                />
                <div className="content">
                    { children }
                </div>
            </div>
        </div>
    )
}

export default Layout;