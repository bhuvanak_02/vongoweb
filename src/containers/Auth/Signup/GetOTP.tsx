import React from 'react';
import {
    Stack,
    FormGroup,
    FormControlLabel,
    Checkbox,
    Divider,
    Typography,
    InputBase,
    Button
} from '@mui/material';
import { makeStyles } from '@mui/styles';
import PhoneIcon from '@mui/icons-material/Phone';
import { useResponsiveScreenState } from 'modules/hooks';

const useStyles = makeStyles({
    phoneIcon: {
        color: '#FD8957 !important',
        margin: '8px 8px 4px 8px',
        fontSize: '20px !important'
    },
    phoneInputContainer: {
        borderRadius: '30px !important',
        backgroundColor: '#FFFFFF',
        color: '#000000 !important',
        fontSize: '16px',
        alignItems: 'center',
        padding: '12px',
        marginTop: '8px',
        marginBottom: '8px'
    },
    phoneInput: {
        maxWidth: '100%'
    },
    phoneInputWrapper: ({ isTinyMobile }) => ({
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        flexWrap: 'wrap',
        margin: isTinyMobile ? '4px auto' : '24px auto'
    }),
    getOTPBtn: {
        backgroundColor: '#FD8957 !important',
        borderRadius: '30px !important',
        padding: '12px 16px !important',
        'text-transform': 'none !important',
        fontSize: '16px !important',
        'font-weight': '400 !important',
        marginLeft: '8px !important',
        marginTop: '8px !important',
        marginBottom: '8px !important'
    },
    termsCheckbox: {

    }
});

export default function GetOTP() {

    const { isTinyMobile } = useResponsiveScreenState();
    const classes = useStyles({isTinyMobile});

    function handleAcceptTerms(event, checked) {
        console.log('Accept terms : ', checked);
    }

    function TermsLabel() {
        return (
            <Typography
                sx={{ fontSize: '14px !important' }}
            >
                By signing up, you agree to our Terms and
                Conditions and Privacy Policy and Community
                Guidelines
            </Typography>
        );
    }

    function handleGetOTPClick() {}

    return (
        <Stack>
            <div className={classes.phoneInputWrapper}>
                <Stack className={classes.phoneInputContainer} direction="row">
                    <PhoneIcon className={classes.phoneIcon} />
                    <Typography sx={{ fontSize: 'inherit'}}>+91</Typography>
                    <Divider
                        orientation="vertical"
                        flexItem
                        sx={{ margin: '0 12px', borderRightWidth: 2, borderColor: 'rgba(0, 0, 0, 0.21)' }}
                    />
                    <InputBase placeholder="Phone Number" sx={{ width: '100%', fontSize: '16px' }} />
                </Stack>
                <Button
                    className={classes.getOTPBtn}
                    variant="contained"
                    onClick={handleGetOTPClick}
                >
                    Get OTP
                </Button>
            </div>
            <FormGroup sx={{ mt: isTinyMobile ? 0 : 2, my: isTinyMobile ? 0 : 4, textAlign: 'left' }}>
                <FormControlLabel
                    control={
                        <Checkbox
                            sx={{
                                backgroundColor: '#FFFFF !important',
                                mr: isTinyMobile ? 0 : 2, 
                                color: '#FD8957 !important'
                            }}
                            onChange={handleAcceptTerms}
                            size={isTinyMobile ? 'small': 'medium'}
                        />
                    }
                    sx={{ mb: 2, alignItems: 'flex-start' }}
                    label={<TermsLabel />}
                />
            </FormGroup>
        </Stack>
    )
}