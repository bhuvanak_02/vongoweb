import React, { useEffect, useState, useRef } from 'react';
import { withRouter } from 'react-router-dom';
import { Box, Modal, Typography, Slide } from '@mui/material';
import { TransitionGroup } from 'react-transition-group';
import Logo from 'components/Sidebar/vongo_logo.png';
import GetOTP from './GetOTP';
import './styles.scss';

function Signup({
    match: { path }
}) {
    const [currentStep, setCurrentStep] = useState(0);
    const authContainerRef = useRef(null);

    useEffect(() => {
        
    }, []);


    return (
        <Modal open={true}>
            <Box className="auth-wrapper">
                <Box className="auth-container" ref={authContainerRef}>
                    <img src={Logo} alt='Vongo' className="logo" />
                    <Typography variant="h1" gutterBottom className="title">Sign Up</Typography>
                    <Typography variant="subtitle1" className="sub-title" gutterBottom>Please Login to Continue</Typography>
                    <TransitionGroup>
                        { currentStep === 0  ? (
                            <Slide
                                direction='left'
                                container={authContainerRef.current}
                                timeout={{ enter: 500, exit: 0}}
                            >
                                <Box sx={{maxWidth: '80%', margin: 'auto'}}>
                                    <GetOTP />
                                </Box>
                            </Slide>) : null
                        }
                        { currentStep === 1 ? (
                            <Slide
                                direction='left'
                                container={authContainerRef.current}
                                timeout={{ enter: 500, exit: 0}}
                            >
                                <p>Test 2</p>
                            </Slide>) : null
                        }
                    </TransitionGroup>
                </Box>
            </Box>
        </Modal>
    )
}

export default withRouter(Signup);