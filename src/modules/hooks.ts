/* eslint-disable import/prefer-default-export */
import { shallowEqual, useSelector } from 'react-redux';
import { useMediaQuery } from '@mui/material';

import { StoreState } from 'types';

export function useShallowEqualSelector<TReturn>(selector: (state: StoreState) => TReturn) {
  return useSelector(selector, shallowEqual);
}

export function useResponsiveScreenState() {
  const isTinyMobile = useMediaQuery('(max-width: 360px');
  const isTabOrMobile = useMediaQuery('(min-width: 360px) and (max-width: 1200px)');
  const isDesktop = !isTinyMobile && !isTabOrMobile;
  return {
    isTinyMobile,
    isTabOrMobile,
    isDesktop
  };
}
