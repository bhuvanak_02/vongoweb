import SpinnerSlice from './SpinnerSlice';
import RoomSlice from './RoomSlice';
import ClubsSlice from './ClubsSlice';
import PeopleSlice from './PeopleSlice';

export {
    SpinnerSlice,
    RoomSlice,
    ClubsSlice,
    PeopleSlice
};
