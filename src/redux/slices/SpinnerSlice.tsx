import {createSlice} from '@reduxjs/toolkit';

const spinnerSlice = createSlice({
  name: 'spinner',
  initialState: {
    isLoading: false,
    showOverlay: false,
  },
  reducers: {
    setIsLoading: (state, action) => {
      state.isLoading = action.payload.isLoading;
      state.showOverlay = action.payload.showOverlay;
    },
  },
});

export const showSpinner =
  (isLoading, showOverlay = false) =>
  async dispatch => {
    dispatch(
      spinnerSlice.actions.setIsLoading({
        isLoading: isLoading,
        showOverlay: showOverlay,
      }),
    );
  };

export default spinnerSlice;
