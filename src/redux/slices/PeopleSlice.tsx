import {createSlice} from '@reduxjs/toolkit';
import Axios from 'axios';
import {showSpinner} from './SpinnerSlice';

const peopleSlice = createSlice({
  name: 'people',
  initialState: {
    peoplesList: [],
    initialRender: false,
  },
  reducers: {
    storePeoplesListList(state, action) {
      state.peoplesList = action.payload;
    },
    storeInitialRender(state, action) {
      state.initialRender = action.payload;
    },
  },
});

export const getPeoplesList = () => async (dispatch) => {
  dispatch(showSpinner(true));
  await Axios.post('https://vongo.incusquiz.com/vongo/user/getAll')
    .then(resp => {
        console.log(resp, '9898989')
      dispatch(peopleSlice.actions.storePeoplesListList(resp.data.userLst));
      dispatch(peopleSlice.actions.storeInitialRender(true));
      dispatch(showSpinner(false));
    })
    .catch(error => {
        console.log(error, 'error')
      dispatch(showSpinner(false));
    });
};

export default peopleSlice;
