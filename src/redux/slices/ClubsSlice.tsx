import {createSlice} from '@reduxjs/toolkit';
import Axios from 'axios';
import {showSpinner} from './SpinnerSlice';

const clubsSlice = createSlice({
  name: 'clubs',
  initialState: {
    clubsList: [],
    initialRender: false,
  },
  reducers: {
    storeClubsList(state, action) {
      state.clubsList = action.payload;
    },
    storeInitialRender(state, action) {
      state.initialRender = action.payload;
    },
  },
});

export const getClubsList = () => async (dispatch) => {
  dispatch(showSpinner(true));
  await Axios.post('https://vongo.incusquiz.com/vongo/club/getAll')
    .then(resp => {
      dispatch(clubsSlice.actions.storeClubsList(resp.data.clubDetails));
      dispatch(clubsSlice.actions.storeInitialRender(true));
      dispatch(showSpinner(false));
    })
    .catch(error => {
        console.log(error, 'error')
      dispatch(showSpinner(false));
    });
};

export default clubsSlice;
