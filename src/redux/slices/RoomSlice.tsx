import {createSlice} from '@reduxjs/toolkit';
import Axios from 'axios';
import {showSpinner} from './SpinnerSlice';

const roomSlice = createSlice({
  name: 'room',
  initialState: {
    roomsListData: [],
    initialRender: false,
  },
  reducers: {
    storeRoomsList(state, action) {
      state.roomsListData = action.payload;
    },
    storeInitialRender(state, action) {
      state.initialRender = action.payload;
    },
  },
});

export const getRoomsList = () => async (dispatch) => {
  dispatch(showSpinner(true));
  await Axios.post('https://vongo.incusquiz.com/vongo/room/getAll')
    .then(resp => {
      dispatch(roomSlice.actions.storeRoomsList(resp.data.roomDetailsLst));
      dispatch(roomSlice.actions.storeInitialRender(true));
      dispatch(showSpinner(false));
    })
    .catch(error => {
        console.log(error, 'error')
      dispatch(showSpinner(false));
    });
};

export default roomSlice;
