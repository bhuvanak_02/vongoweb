import { configureStore, combineReducers, getDefaultMiddleware } from '@reduxjs/toolkit';
import { SpinnerSlice, RoomSlice, ClubsSlice, PeopleSlice } from './slices';

const rootReducer = combineReducers({
  spinner: SpinnerSlice.reducer,
  room: RoomSlice.reducer,
  clubs: ClubsSlice.reducer,
  people: PeopleSlice.reducer,
});

const store = configureStore({
  reducer: rootReducer,
  middleware: getDefaultMiddleware({
    serializableCheck: false,
  }),
  //.concat(logger),
  devTools: false,
  // enhancers: [devToolsEnhancer({realtime: true, secure: false})],
});

export default store;
